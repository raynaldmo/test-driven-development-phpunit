## Test-Driven Development in PHP with PHPUnit (lynda.com)
### Course Notes

### Ch01 - Basics of Test-Driven Development

* Why Unit test ?
  * Ensures code works correctly
  * Provides additional documentation
  * Reduces the chance of bugs
  * Improves the ability to refactor
  * Helps to write better designed code
  

#### Installing and Using PhpUnit
```bash
# Installation
$ composer require phpunit/phpunit  

# running tests
# run all tests under directory tests/
$ vendor/bin/phpunit tests

# run single test
$ vendor/bin/phpunit tests/ReceiptTest.php 

# run specific method
$ vendor/bin/phpunit tests --filter=testTax

$ vendor/bin/phpunit tests --filter=ReceiptTest::testTax

# run with phpunit.xml file
# create phpunit.xml file
# execute phpunit to run all test suites
$ vendor/bin/phpunit

# run specific test suite
$ vendor/bin/phpunit --testsuite=app

# run specific method in test suite
$ vendor/bin/phpunit --testsuite=app --filter=testTax

```