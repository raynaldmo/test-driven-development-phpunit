<?php

namespace TDD;

class Receipt {
  // calculate total
  public function total(array $items = []) {
    return array_sum($items);
  }

  // calculate tax
  public function tax($amount, $tax) {
    return ($amount * $tax);
  }
}
