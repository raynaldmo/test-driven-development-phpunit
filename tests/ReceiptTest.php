<?php
namespace TDD\Test;

echo dirname(__FILE__) . PHP_EOL;
echo dirname(dirname(__FILE__)) .PHP_EOL;

require dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR .'autoload.php';

// Explicitly load class file. It's not being auto-loaded and am not sure it's
// supposed to be.
require dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR .'Receipt.php';

use PHPUnit\Framework\TestCase;
use TDD\Receipt;

class ReceiptTest extends TestCase {

  private $Receipt;

  // Automatically called by PhpUnit before each test method
  public function setUp() {
    $this->Receipt = new Receipt();
  }

  // Test total method
  public function testTotal() {
		$input = [0,2,5,8];
		$output = $this->Receipt->total($input);
    $this->assertEquals(
			15,
			$output,
			'When summing the total should equal 15'
		);
	}

	// Test tax method
	public function testTax() {
    $inputAmount = 10.00;
    $taxInput = 0.10;
    $output = $this->Receipt->tax($inputAmount, $taxInput);
    $this->assertEquals(
      1.00,
      $output,
      'The tax calculation should equal 1.00'
    );
  }

  // Automatically called by PhpUnit after each test method
  public function tearDown() {
    unset($this->Receipt);
  }
}